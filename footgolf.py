import requests

competitors = requests.get("https://www.wth.netacad.hu/api/footgolf").json() # download data

for i in range(len(competitors)): # "fixing" competitors' id
    competitors[i]["id"] = str(i)

# 3 number of competitors
print("3. feladat: Versenyzők száma: " + str(len(competitors)))

# 4. women to everyone rate
female = 0
for competitor in competitors:
    if competitor["category"] == "Noi":
        female += 1
print("4. feladat: A női versenyzők aránya: " + str(round((female / len(competitors)) * 100, 2)) + "%")

# 5. calc the final score
for competitor in competitors:
    scores = []
    for i in range(1, 9):
        scores.append(competitor["r" + str(i)])
    lowest = [255, 255] # search for 2 lowest & summarize score
    maxScore = 0
    for score in scores:
        lowest.sort()
        if lowest[1] > score:
            lowest[1] = score
        maxScore += score
    for i in range(2): # bonus for participation
        if lowest[i] != 0:
            lowest[i] -= 10
    finalScore = maxScore - (lowest[0] + lowest[1]) # calc fianl score
    competitors[int(competitor["id"])]["score"] = finalScore

# 6. guess the best woman competitor
bestId = 0
for competitor in competitors:
    if competitor["category"] != "Noi":
        continue
    id = int(competitor["id"])
    if competitors[bestId]["score"] < competitor["score"]:
        bestId = int(competitor["id"])
best = competitors[bestId]
print("6. feladat: A bajnok női versenyző \n\tNév: " + best["name"] + "\n\tEgyesület: " + best["team"] + "\n\tÖsszpont: " + str(best["score"]))

# 7. write men's score to a file
data = ""
for competitor in competitors:
    data += competitor["name"] + ";" + str(competitor["score"]) + "\n"
open("osszpontFF.txt", "w").write(data.strip())

#8. teams stat
teams = {}
for competitor in competitors:
    name = competitor["team"]
    if name == "n.a.":
        continue
    try:
        teams[name] += 1
    except KeyError:
        teams[name] = 1
teamsInfo = "8. feladat: Egyesület statisztika\n"
for name in teams:
    if teams[name] > 2:
        teamsInfo += "\t" + name + " - " + str(teams[name]) + " fő\n"
print(teamsInfo.strip())